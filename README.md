# ci-templates

Helm chart basic gitlab CI

```yaml
cache:
- key: ${CI_COMMIT_REF_NAME}

default:
  tags: 
  - amd64

include:
- project: momentum-managed-public/ci-templates
  ref: main
  file: .gitlab/ci/helm/.harbor.gitlab-ci.yml
- project: momentum-managed-public/ci-templates
  ref: main
  file: .gitlab/ci/compliance/.validate_merge_request_commits.gitlab-ci.yml
- project: momentum-managed-public/ci-templates
  ref: main
  file: .gitlab/ci/security/.sast.gitlab-ci.yml

stages:
- validate-merge-request
- sast
- helm-publish
- helm-release
# # When publishing an external product
# - helm-contribute

validate-merge-request:
  extends: .validate_merge_request_commits

secrets-leaks:
  extends: .secrets_leaks

semgrep-sast:
  extends: .semgrep_sast

helm-publish:
  extends: .helm_publish

helm-release:
  extends: .helm_release

# # When publishing an external product
# helm-contribute:
#   extends: .helm_contribute
```
